var express = require('express');
const Doc_apis = require('../db/models/Doc_apis');
const Endpoint = require('../db/models/Endpoint');

var router = express.Router();
const introductions = require('../db/models/Introduction');
const users = require('../db/models/User'); 
const Versions = require('../db/models/Versions');

router.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");
    next();
  });

  /////////////////////////User////////////////////////////////////////
  router.get('/user', (req, res) => {
    users.find({})
        .then(user => {
            res.status(200).json(user)
        })
        .catch(err => {
            console.log('Error: ', err);
            throw err;
        })
});
router.post('/user',async (req, res) => {
    console.log(req.body)
    const newUser = new users({
        Id: req.body.Id,
        Username: req.body.Username,
        password: req.body.password,
        lastName: req.body.lastName,
        firstName: req.body.firstName,
        doc_type: req.body.doc_type
    });

   let result = await newUser.save()
       console.log(result),
       res.send(result)
});

router.get('/user/:id', async (req, res) => {
    try {
        var user = await users.findById(req.params.id).exec();
        res.send(user);
    } catch (error) {
        res.status(500).send(error);
    }
  });

  router.put('/user/:_id', async (req, res) => {
    var userId = {_id:req.params._id};
    users.update(userId, req.body)
    .then(doc=>{
        if(!doc){
            return res.status(404).end()
        }
        return res.status(204).end()
    })
    .catch(err=>next(err))
})
router.delete('/user/:id',function (req, res) {
    users
    .findByIdAndRemove(req.params.id)
    .exec()
    .then(doc=>{
     if(!doc){
         return res.status(404).end()
     }
     return res.status(204).end()
    })
    .catch(err=>next(err))
 });

//------------------------------------Versions------------------------------------------------
router.get('/version', (req, res) => {
    Versions.find({})
        .then(version => {
            res.status(200).json(version)
        })
        .catch(err => {
            console.log('Error: ', err);
            throw err;
        })
});
router.post('/version',async (req, res) => {
    console.log(req.body)
    const newVersion = new Versions({
        Id: req.body.Id,
        version: req.body.version,
        description: req.body.description,
        created_by: req.body.created_by,
    });

   let result = await newVersion.save()
       console.log(result),
       res.send(result)
});

router.put('/version/:_id', async (req, res) => {
    var versionId = {_id:req.params._id};
    Versions.update(versionId, req.body)
    .then(doc=>{
        if(!doc){
            return res.status(404).end()
        }
        return res.status(204).end()
    })
    .catch(err=>next(err))
})

router.get('/version/:id', async (req, res) => {
    try {
        var version = await Versions.findById(req.params.id).exec();
        res.send(version);
    } catch (error) {
        res.status(500).send(error);
    }
  });

  router.delete('/version/:id',function (req, res) {
    Versions
    .findByIdAndRemove(req.params.id)
    .exec()
    .then(doc=>{
     if(!doc){
         return res.status(404).end()
     }
     return res.status(204).end()
    })
    .catch(err=>next(err))
 });
//------------------------docapi---------------------------------

router.get('/docApi', async (req, res) => {
    Doc_apis.find({})
        .then(docapi => {
            res.status(200).json(docapi)
        })
        .catch(err => {
            console.log('Error: ', err);
            throw err;
        })
});
router.get('/docApi/:id', async (req, res) => {
    try {
        var query = {idVersion:req.params.id}
        var docApi = await Doc_apis.find(query).exec();
        res.send(docApi);
    } catch (error) {
        res.status(500).send(error);
    }
});
router.get('/docApi/:idversion/:iddocAPi', async (req, res) => {
    try {
       
        var query = {idVersion:req.params.idversion, _id:req.params.iddocAPi}
        console.log(query)
        var docApi = await Doc_apis.find(query).exec();
        res.send(docApi);
    } catch (error) {
        res.status(500).send(error);
    }
});
router.post('/docApi',async (req, res) => {
    
    const newDocApi = new Doc_apis({
        idVersion: req.body.idVersion,
        idEndPoint: req.body.idEndPoint,
        doc_type: req.body.doc_type,
        name: req.body.name,
        desc: req.body.desc,
        enpoint: req.body.enpoint,
        method: req.body.method,
        params_request: req.body.params_request,
        decs_request: req.body.decs_request,
        response: req.body.response,
        response_request: req.body.response_request
    });
   let result = await newDocApi.save()
       console.log(result),
       res.send(result)
});
module.exports = router;

router.delete('/docapi/:id',function (req, res) {
    Doc_apis
    .findByIdAndRemove(req.params.id)
    .exec()
    .then(doc=>{
     if(!doc){
         return res.status(404).end()
     }
     return res.status(204).end()
    })
    .catch(err=>next(err))
 });

 router.put('/docapi/:id', async (req, res) => {
    var docapiId = {_id:req.params.id};
    console.log(docapiId);
    console.log(req.body);
    Doc_apis.update(docapiId, req.body)
    .then(doc=>{
        if(!doc){
            return res.status(404).end()
        }
        return res.status(204).end()
    })
    .catch(err=>console.log(err))
})
//----------------------endpoint-----------------------
router.get('/endpoint', async (req, res) => {
    Endpoint.find({})
        .then(endpoint => {
            res.status(200).json(endpoint)
        })
        .catch(err => {
            console.log('Error: ', err);
            throw err;
        })
});
router.post('/endpoint',async (req, res) => {
    
    const newEndpoint = new Endpoint({
        Id: req.body.Id,
        type: req.body.type,
        desc: req.body.desc,
        endpoint_dev: req.body.endpoint_dev,
        endpoint_pro: req.body.endpoint_pro,
        header: req.body.header,
        response: req.body.response,
    });
   let result = await newEndpoint.save()
       console.log(result),
       res.send(result)
});

router.get('/endpoint/:_id', async (req, res) => {
    try {
       
        var query = {_id:req.params._id}
        var endpoint = await Endpoint.find(query).exec();
        res.send(endpoint);
    } catch (error) {
        res.status(500).send(error);
    }
});


router.delete('/endpoint/:id',function (req, res) {
    Endpoint
    .findByIdAndRemove(req.params.id)
    .exec()
    .then(doc=>{
     if(!doc){
         return res.status(404).end()
     }
     return res.status(204).end()
    })
    .catch(err=>next(err))
 });

 router.put('/endpoint/:id', async (req, res) => {
    var endpointId = {_id:req.params.id};
    Endpoint.update(endpointId, req.body)
    .then(doc=>{
        if(!doc){
            return res.status(404).end()
        }
        return res.status(204).end()
    })
    .catch(err=>console.log(err))
})