const mongoose = require("mongoose");
const versionsSchema = mongoose.Schema(
  {
    Id:{
        type: String,
        required: true
    },
     version: {
        type: String,
        required: true
     },
     description: {
        type: String,
        required: true
     },
     created_by: {
        type: String,
        required: true
     },
  },
  { timestamps: true, strict: true }
);

module.exports = mongoose.model("versions",versionsSchema);