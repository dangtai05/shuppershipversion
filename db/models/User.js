const mongoose = require("mongoose");
const userSchema = mongoose.Schema(
  {
    Id:{
        type: String,
        required: true
    },
     Username: {
        type: String,
        required: true
     },
     password: {
        type: String,
        required: true
     },
     lastName: {
        type: String,
        required: true
     },
     firstName: {
        type: String,
        required: true
     },
     doc_type:{
      type: String,
      required: true
     }
   },
  { timestamps: true, strict: true }
);

module.exports = mongoose.model("user",userSchema);