const { data } = require("jquery");
const mongoose = require("mongoose");
const docApisSchema = mongoose.Schema(
  {
    idVersion: {
      type: String,
      required: true,
    },
    idEndPoint: {
      type: String,
      required: true,
    },
    doc_type: {
      type: String,
      required: true,
    },
    name: {
      type: String,
      required: true,
    },
    desc: {
      type: String,
      required: true,
    },
    enpoint: {
      type: String,
      required: true,
    },
    method: {
      type: String,
      required: true,
    },
    params_request: [
      {
        name_param: {
          type: String,
          default_value: null,
        },
        type_param: {
          type: String,
          default_value: null,
        },
        require_param: {
          type: String,
          default_value: null,
        },
        decs_param: {
          type: String,
          default_value: null,
        },
      },
    ],
    decs_request: [
        {
        type: String,
        default_value: null,
        }
    ],
    response: [
      {
        name_response: {
          type: String,
          default_value: null,
        },
        type_response: {
          type: String,
          default_value: null,
        },
        decs_response: {
          type: String,
          default_value: null,
        },
      },
    ],
    response_request: [
      {
        success: {
          type: String,
          default_value: null,
        },
        message: {
          type: String,
          default_value: null,
        },
        response_request_data: [{
          type: String,
          default_value: null,
        }],
      },
    ],
  },
  { timestamps: true, strict: true }
);

module.exports = mongoose.model("docApis", docApisSchema);
