const mongoose = require("mongoose");
const endpointSchema = mongoose.Schema(
  {
    Id:{
      type: String,
      required: true
    },
     type: {
        type: String,
        required: true
     },
     desc: {
        type: String,
        required: true
     },
     endpoint_dev: {
        type: String,
        required: true
     },
     endpoint_pro: {
        type: String,
        required: true
     },
     header: [
        {
          email: {
            type: String,
            default_value: null,
          },
          password: {
            type: String,
            default_value: null,
          }
        },
      ],
      response: [
        {
          name_response: {
            type: String,
            default_value: null,
          },
          type_response: {
            type: String,
            default_value: null,
          },
          decs_response: {
            type: String,
            default_value: null,
          },
        },
      ]
  },
  { timestamps: true, strict: true }
);

module.exports = mongoose.model("endpoints",endpointSchema);